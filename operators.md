
$inc
 Increments the value of the field by the specified amount.
$min
 Only updates the field if the specified value is less than the existing field value.
$max
 Only updates the field if the specified value is greater than the existing field value.
$mul
 Multiplies the value of the field by the specified amount.
$rename
 Renames a field.
$set
 Sets the value of a field in a document.
$pop
 Removes the first or last item of an array.
$pull
 Removes all array elements that match a specified query.
$push
 Adds an item to an array.
$pullAll
 Removes all matching values from an array.